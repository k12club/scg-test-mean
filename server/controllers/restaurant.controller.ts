import Restaurant from '../models/restaurant.model.ts';
import BaseCtrl from './base';
import axios from 'axios'
class RestaurantCtrl extends BaseCtrl {
  model = Restaurant;

  getLocation = async (req, res) => {
    const { location, radius } = req.body

    const result = await axios.post(`http://203.113.11.192:3000/latnlgapi`, { location })
    const { lat, lng } = result.data.results[0].geometry.location
    const data = await this.model.find({})
    const results = data.filter((res: any) => {
      const distance = this.Distance(res.lat, res.lng, lat, lng, "K")
      // console.log(distance, radius,res.name);

      return distance <= radius
    })
    res.json({ results, marker: { lat, lng } })
  }
  Distance = (lat1, lon1, lat2, lon2, unit) => {
    if ((lat1 == lat2) && (lon1 == lon2)) {
      return 0;
    }
    else {
      var radlat1 = Math.PI * lat1 / 180;
      var radlat2 = Math.PI * lat2 / 180;
      var theta = lon1 - lon2;
      var radtheta = Math.PI * theta / 180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = dist * 180 / Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit == "K") { dist = dist * 1.609344 }
      if (unit == "N") { dist = dist * 0.8684 }
      return dist;
    }
  }

}

export default RestaurantCtrl;
