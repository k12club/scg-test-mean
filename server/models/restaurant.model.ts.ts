import { model, Schema } from 'mongoose';

const restaurantSchema = new Schema({
  "id": String,
  "name": String,
  "detail": String,
  "headlines": String,
  "phonenumber": String,
  "location": String,
  "type": String,
  "notification": String,
  "create_by": String,
  "view": String,
  "created_at": String,
  "updated_at": String,
  "status": String,
  "pin": String,
  "lat": Number,
  "lng": Number,
  "province": String,
  "like": String,
});

const Restaurant = model('Restaurant', restaurantSchema);

export default Restaurant;

// Generated by https://quicktype.io
export class restaurant {

  "_id": string;
  "id": string;
  "name": string;
  "detail": string;
  "headlines": string;
  "phonenumber": string;
  "location": string;
  "type": string;
  "notification": string;
  "create_by": string;
  "view": string;
  "created_at": string;
  "updated_at": string;
  "status": string;
  "pin": string;
  "lat": string;
  "lng": string;
  "province": string;
  "like": string;

}