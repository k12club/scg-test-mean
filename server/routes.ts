import * as express from 'express';

import CatCtrl from './controllers/cat';
import RestaurantCtrl from './controllers/restaurant.controller';
import UserCtrl from './controllers/user';

function setRoutes(app): void {
  const router = express.Router();
  const restaurantCtrl = new RestaurantCtrl();
  const catCtrl = new CatCtrl();
  const userCtrl = new UserCtrl();

  // Cats
  router.route('/cats').get(catCtrl.getAll);
  router.route('/cats/count').get(catCtrl.count);
  router.route('/cat').post(catCtrl.insert);
  router.route('/cat/:id').get(catCtrl.get);
  router.route('/cat/:id').put(catCtrl.update);
  router.route('/cat/:id').delete(catCtrl.delete);
  // restaurants
  router.route('/restaurants').get(restaurantCtrl.getAll);
  router.route('/restaurants/count').get(restaurantCtrl.count);
  router.route('/restaurant').post(restaurantCtrl.insert);
  router.route('/restaurant/location').post(restaurantCtrl.getLocation);
  router.route('/restaurant/:id').get(restaurantCtrl.get);
  router.route('/restaurant/:id').put(restaurantCtrl.update);
  router.route('/restaurant/:id').delete(restaurantCtrl.delete);

  // Users
  router.route('/login').post(userCtrl.login);
  router.route('/users').get(userCtrl.getAll);
  router.route('/users/count').get(userCtrl.count);
  router.route('/user').post(userCtrl.insert);
  router.route('/user/:id').get(userCtrl.get);
  router.route('/user/:id').put(userCtrl.update);
  router.route('/user/:id').delete(userCtrl.delete);

  // Apply the routes to our application with the prefix /api
  app.use('/api', router);

}

export default setRoutes;
