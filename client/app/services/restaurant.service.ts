import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { restaurant } from 'server/models/restaurant.model.ts';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<restaurant[]>(`/api/restaurants`)
  }
  getAllByLocation(body) {
    return this.http.post<any>(`/api/restaurant/location`, body)
  }
}
