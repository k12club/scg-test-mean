import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { RestaurantService } from 'client/app/services/restaurant.service';
import { GeoLocationService } from 'client/app/services/geo-location.service';

@Component({
  selector: 'app-map-restaurant',
  templateUrl: './map-restaurant.component.html',
  styleUrls: ['./map-restaurant.component.scss']
})
export class MapRestaurantComponent implements OnInit {
  // google maps zoom level
  constructor(private _restaurantService: RestaurantService) {
    if (navigator) {
      navigator.geolocation.getCurrentPosition(pos => {
        this.userMarker = {
          lat: pos.coords.latitude,
          lng: pos.coords.longitude,
          // label: {
          //   color: 'red',
          //   fontFamily: '',
          //   fontSize: '14px',
          //   fontWeight: 'bold',
          //   text: "",

          // },
          draggable: false,
          icon: {
            url: 'https://www.wisible.com/wp-content/uploads/2019/08/avatar-human-male-profile-user-icon-518358.png',
            scaledSize: { width: 40, height: 40, anchor: { x: 19, y: 19 } },
            labelOrigin: { x: 12, y: 50 }

          }
        }
        this.lng = +pos.coords.longitude;
        this.lat = +pos.coords.latitude;
        this.origin = {
          lat: pos.coords.latitude,
          lng: pos.coords.longitude,
        }

      });

    }
  }
  zoom: number = 8;
  imageUrl = 'https://npcrimage-all.netpracharat.com/imagegood/'
  searchText = ''
  // initial center position for the map
  lat: number
  lng: number
  radius = 20
  place = 'บางซื่อ'
  coordinates;
  userMarker
  public origin: any;
  public destination: any;
  clickedMarker(event) {
    const { lat, lng } = event
    this.clicked = true
    this.destination = { lat, lng }
    console.log(this.destination, this.origin)

  }
  clicked = false

  mapClicked($event: MouseEvent) {
    // this.clicked = true
    console.log($event);
  }
  ngOnInit() {
    this.getAllBylocation()
    // https://www.wisible.com/wp-content/uploads/2019/08/avatar-human-male-profile-user-icon-518358.png

  }
  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  markers: marker[] = [
    // {
    //   lat: 51.373858,
    //   lng: 7.215982,
    //   label: {
    //     color: 'red',
    //     fontFamily: '',
    //     fontSize: '14px',
    //     fontWeight: 'bold',
    //     text: "some text",

    //   },
    //   draggable: false,
    //   icon: {
    //     url: 'https://static.thenounproject.com/png/2391755-200.png',
    //     scaledSize: { width: 40, height: 40, anchor: { x: 19, y: 19 } },
    //     labelOrigin: { x: 12, y: 50 }

    //   }
    // },
  ]
  markersFinded


  getAllBylocation() {
    this.markers = []
    const body = { location: this.place, radius: this.radius }
    this._restaurantService.getAllByLocation(body)
      .subscribe(result => {
        console.log();
        const { lat, lng } = result.marker

        const mapdata = result.results.map(res => (
          this.markers.push({
            _id: res._id,
            lat: parseFloat(res.lat),
            lng: parseFloat(res.lng),
            // label: {
            //   color: 'red',
            //   fontFamily: 'Prompt',
            //   fontSize: '14px',
            //   fontWeight: '',
            //   // text: res.name.length < 10 ? res.name : res.name.substring(0, 9) + '...'
            //   text:  res.name
            // },
            icon: {
              url: `${this.imageUrl}${res.headlines}`,
              scaledSize: { width: 60, height: 60, anchor: { x: 19, y: 19 } },
              labelOrigin: { x: 12, y: 50 }
            },
            draggable: false,
            name: res.name

          })

        ))
        this.markersFinded = {
          lat: parseFloat(lat),
          lng: parseFloat(lng),
          name: 'ตำแหน่งที่คุณค้นหา',

          draggable: false,
        }

        console.log(this.markersFinded, 'markersFinded');

      })
  }
}

// just an interface for type safety.


interface marker {
  _id?: string,
  lat?: number,
  lng?: number,
  label?: {
    color: string,
    fontFamily: string,
    fontSize: string,
    fontWeight: string,
    text: string,

  },
  draggable?: boolean,
  icon?: {
    url: string,
    scaledSize: { width: number, height: number, anchor: { x: number, y: number } },
    labelOrigin: { x: number, y: number }

  },
  name?: string
}

export enum fontWeight {
  'bold',
}
export enum draggable {
  true, false
}

