import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MapRestaurantComponent } from './map-restaurant.component';

const routes: Routes = [
  { path: '', redirectTo: 'map', pathMatch: 'full' },
  { path: 'map', component: MapRestaurantComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapRestaurantRoutingModule { }
