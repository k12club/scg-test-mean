import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapRestaurantRoutingModule } from './map-restaurant-routing.module';
import { MapRestaurantComponent } from './map-restaurant.component';
import { AgmCoreModule } from '@agm/core';
import { SharedModule } from 'client/app/shared/shared.module';
import { GeoLocationService } from 'client/app/services/geo-location.service';
import { AgmDirectionModule } from 'agm-direction';   // agm-direction


@NgModule({
  declarations: [
    MapRestaurantComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    MapRestaurantRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBmSihlcbMmFXVLETeDQwN4kGCAs6M0TrE'
    }),
    AgmDirectionModule
  ]
  ,providers:[GeoLocationService]
})
export class MapRestaurantModule { }
