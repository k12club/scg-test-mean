import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarStyleTwoComponent } from '../components/common/navbar-style-two/navbar-style-two.component';
import { FooterStyleTwoComponent } from '../components/common/footer-style-two/footer-style-two.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    FooterStyleTwoComponent,
    NavbarStyleTwoComponent

  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  exports: [
    FooterStyleTwoComponent,
    NavbarStyleTwoComponent,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule

  ]
})
export class SharedModule { }
